System.register("chunks:///_virtual/capture-example",["./CaptureScene.ts"],(function(){return{setters:[null],execute:function(){}}}));

System.register("chunks:///_virtual/CaptureScene.ts",["./rollupPluginModLoBabelHelpers.js","cc"],(function(t){var e,n,r;return{setters:[function(t){e=t.inheritsLoose},function(t){n=t.cclegacy,r=t._decorator}],execute:function(){var c;n._RF.push({},"aeb524Sd0tFKrGe6rUZ2eAL","CaptureScene",void 0);var o=r.ccclass;r.property,t("CaptureScene",o("CaptureScene")(c=function(t){function n(){for(var e,n=arguments.length,r=new Array(n),c=0;c<n;c++)r[c]=arguments[c];return(e=t.call.apply(t,[this].concat(r))||this).content=null,e}e(n,t);var r=n.prototype;return r.onEnter=function(){this.content=this.getNodeByName("content"),this.registerButtonClick("btnFullscreen",this.onClickFullscreen),this.registerButtonClick("btnNode",this.onClickNode),this.registerButtonClick("btnClear",this.onClickClear)},r.onClickFullscreen=function(){tnt.captureMgr.captureScreenAsync().parent=this.content},r.onClickNode=function(){var t=this.getNodeByName("Sprite");tnt.captureMgr.captureNodeAsync(t).parent=this.content},r.onClickClear=function(){for(var t=this.content.children.length;t--;)tnt.captureMgr.recycleCaptureNode(this.content.children[t])},n}(tnt.SceneBase))||c);n._RF.pop()}}}));

(function(r) {
  r('virtual:///prerequisite-imports/capture-example', 'chunks:///_virtual/capture-example'); 
})(function(mid, cid) {
    System.register(mid, [cid], function (_export, _context) {
    return {
        setters: [function(_m) {
            var _exportObj = {};

            for (var _key in _m) {
              if (_key !== "default" && _key !== "__esModule") _exportObj[_key] = _m[_key];
            }
      
            _export(_exportObj);
        }],
        execute: function () { }
    };
    });
});