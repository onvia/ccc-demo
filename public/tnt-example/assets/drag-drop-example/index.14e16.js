System.register("chunks:///_virtual/drag-drop-example",["./DragDropScene.ts"],(function(){return{setters:[null],execute:function(){}}}));

System.register("chunks:///_virtual/DragDropScene.ts",["./rollupPluginModLoBabelHelpers.js","cc"],(function(r){var t,n,o,e,a,i,c,g;return{setters:[function(r){t=r.inheritsLoose},function(r){n=r.cclegacy,o=r._decorator,e=r.Node,a=r.color,i=r.randomRangeInt,c=r.Sprite,g=r.instantiate}],execute:function(){var s;n._RF.push({},"53976hOxalNFpSnlDiZ01SV","DragDropScene",void 0);var u=o.ccclass;o.property,r("DragDropScene",u("DragDropScene")(s=function(r){function n(){return r.apply(this,arguments)||this}t(n,r);var o=n.prototype;return o.onEnter=function(){var r=this.find("dragArea");r.draggable=!0,r.on(e.DragEvent.DRAG_START,this.onDragAreaDragStart,this)},o.onDragAreaDragStart=function(r){var t=r.currentTarget;t.stopDrag(),t.parent.startDrag()},o.onEnterTransitionWillFinished=function(r){var t=this.find("dragNode"),n=this.find("container"),o=this.find("TouchPanel");this.find("dragLabel").draggable=!0,tnt.dragDropMgr.on(this,!0,o),tnt.dragDropMgr.addContainer(n),this.registerButtonClick("Button",(function(){console.log("DragDropScene-> click Button")})),tnt.dragDropMgr.registerDragTarget(t,0),this.find("content").children.forEach((function(r){r.sprite.color=a(i(0,255),i(0,255),i(0,255)),tnt.dragDropMgr.registerDragTarget(r,.2,(function(r){var t=r.currentTarget;console.log("DragDropScene-> click ",t.name),t.sprite.color=a(i(0,255),i(0,255),i(0,255))}))}))},o.onExitTransitionStart=function(r){tnt.dragDropMgr.off(this)},o.onCreateDragAgentData=function(r,t){var n=r.getComponent(c);return{icon:g(r),sourceData:n.color.clone()}},o.onDropAgent=function(r,t,n){r&&(r.getComponent(c).color=n)},n}(tnt.SceneBase))||s);n._RF.pop()}}}));

(function(r) {
  r('virtual:///prerequisite-imports/drag-drop-example', 'chunks:///_virtual/drag-drop-example'); 
})(function(mid, cid) {
    System.register(mid, [cid], function (_export, _context) {
    return {
        setters: [function(_m) {
            var _exportObj = {};

            for (var _key in _m) {
              if (_key !== "default" && _key !== "__esModule") _exportObj[_key] = _m[_key];
            }
      
            _export(_exportObj);
        }],
        execute: function () { }
    };
    });
});