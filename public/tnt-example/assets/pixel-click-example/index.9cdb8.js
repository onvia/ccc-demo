System.register("chunks:///_virtual/pixel-click-example",["./PixelClickDemo.ts"],(function(){return{setters:[null],execute:function(){}}}));

System.register("chunks:///_virtual/PixelClickDemo.ts",["./rollupPluginModLoBabelHelpers.js","cc"],(function(e){var t,i,n,r,o,c,a,l,s;return{setters:[function(e){t=e.inheritsLoose,i=e.applyDecoratedDescriptor,n=e.initializerDefineProperty,r=e.assertThisInitialized},function(e){o=e.cclegacy,c=e._decorator,a=e.Mat4,l=e.Vec2,s=e.Vec3}],execute:function(){var u,f,h,p,T;o._RF.push({},"faeadNoYThBkbmQgGSwHSbS","PixelClickDemo",void 0);var d=c.ccclass,m=(c.property,tnt._decorator),y=m.node;m.sprite,m.component,new a,new a,new a(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),new l,new l,new s,e("PixelClickDemo",(u=d("PixelClickDemo"),f=y(),u((p=function(e){function i(){for(var t,i=arguments.length,o=new Array(i),c=0;c<i;c++)o[c]=arguments[c];return t=e.call.apply(e,[this].concat(o))||this,n(t,"ClickTest",T,r(t)),t}return t(i,e),i.prototype.onEnterTransitionStart=function(){this.getNodeByName("Layout1").children.forEach((function(e){tnt.hitTest.enablePixelHitTest(e.uiTransform)})),this.getNodeByName("Layout2").children.forEach((function(e){tnt.hitTest.enablePixelHitTest(e.uiTransform)})),this.ClickTest.children.forEach((function(e){tnt.hitTest.enablePixelHitTest(e.uiTransform)}))},i}(tnt.SceneBase),T=i(p.prototype,"ClickTest",[f],{configurable:!0,enumerable:!0,writable:!0,initializer:function(){return null}}),h=p))||h));o._RF.pop()}}}));

(function(r) {
  r('virtual:///prerequisite-imports/pixel-click-example', 'chunks:///_virtual/pixel-click-example'); 
})(function(mid, cid) {
    System.register(mid, [cid], function (_export, _context) {
    return {
        setters: [function(_m) {
            var _exportObj = {};

            for (var _key in _m) {
              if (_key !== "default" && _key !== "__esModule") _exportObj[_key] = _m[_key];
            }
      
            _export(_exportObj);
        }],
        execute: function () { }
    };
    });
});