System.register("chunks:///_virtual/main-scene",["./MainScene.ts"],(function(){return{setters:[null],execute:function(){}}}));

System.register("chunks:///_virtual/MainScene.ts",["./rollupPluginModLoBabelHelpers.js","cc","./SceneConfig.ts"],(function(t){var n,e,i,o,r;return{setters:[function(t){n=t.inheritsLoose},function(t){e=t.cclegacy,i=t._decorator,o=t.instantiate},function(t){r=t.SceneConfig}],execute:function(){var c;e._RF.push({},"891e0QL5GpFSY6ltQDg1nZx","MainScene",void 0);var a=i.ccclass;i.property,t("MainScene",a("MainScene")(c=function(t){function e(){return t.apply(this,arguments)||this}n(e,t);var i=e.prototype;return i.onEnterTransitionStart=function(t){var n=this,e=this.getNodeByName("btnTemplate"),i=e.parent;e.removeFromParent();for(var c=function(){var t=r[a],c=o(e);c.name="btn"+t.scene,c.parent=i;var s=c.getChildByName("Label");n.setLabelText(s,t.button),n.registerButtonClick(c,(function(){tnt.sceneMgr.to(t.scene,{bundle:t.bundle})}))},a=0;a<r.length;a++)c();this.registerButtonClick("btnGitHub",(function(){window.open("https://github.com/onvia/ccc-tnt-framework")})),this.registerButtonClick("btnGitee",(function(){window.open("https://gitee.com/onvia/ccc-tnt-framework")})),e.destroy()},i.onEnterTransitionFinished=function(t){},e}(tnt.SceneBase))||c);e._RF.pop()}}}));

(function(r) {
  r('virtual:///prerequisite-imports/main-scene', 'chunks:///_virtual/main-scene'); 
})(function(mid, cid) {
    System.register(mid, [cid], function (_export, _context) {
    return {
        setters: [function(_m) {
            var _exportObj = {};

            for (var _key in _m) {
              if (_key !== "default" && _key !== "__esModule") _exportObj[_key] = _m[_key];
            }
      
            _export(_exportObj);
        }],
        execute: function () { }
    };
    });
});