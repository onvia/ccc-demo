System.register("chunks:///_virtual/BaseDemo.ts",["./rollupPluginModLoBabelHelpers.js","cc"],(function(e){var t,o,n;return{setters:[function(e){t=e.inheritsLoose},function(e){o=e.cclegacy,n=e._decorator}],execute:function(){var i;o._RF.push({},"ad2e3FGqadKzY5G5PtZApj1","BaseDemo",void 0);var r=n.ccclass;n.property,e("BaseDemo",r("BaseDemo")(i=function(e){function o(){return e.apply(this,arguments)||this}return t(o,e),o.prototype.onEnterTransitionFinished=function(e){var t=this,o=0;this.registerButtonClick("button",(function(){t.setLabelText("labelBtnState","按钮点击: "+o++)})),this.registerToggleGroupEvent("ToggleGroup",(function(e){t.setLabelText("labelToggleState0","选中："+e.node.name)})),this.toggleCheck("ToggleGroup","Toggle3"),this.registerNodeTouchEvent("SpriteTouch",{onTouchBegan:function(e){t.setLabelText("labelSpriteTouchState","触摸状态：按下")},onTouchMoved:function(e){e.getUIStartLocation().subtract(e.getUILocation()).length()>10&&t.setLabelText("labelSpriteTouchState","触摸状态：移动")},onTouchEnded:function(e){t.setLabelText("labelSpriteTouchState","触摸状态：结束")},onTouchCancel:function(e){t.setLabelText("labelSpriteTouchState","触摸状态：取消")}});var n=0;this.registerNodeLongTouchEvent("SpriteLongTouch",.1,(function(){t.setLabelText("labelLongPress","长按："+n++)})),this.registerToggleClick("Toggle",(function(e){t.setLabelText("labelToggleState1","复选状态："+e.isChecked)})),this.registerSliderEvent("Slider",(function(e){t.setLabelText("labelSliderState","滑块进度："+e.progress.toFixed(2))})),this.registerEditBoxDidEnd("EditBox",(function(e){t.setLabelText("labelEditBoxState","输入："+e.string)}))},o}(tnt.SceneBase))||i);o._RF.pop()}}}));

System.register("chunks:///_virtual/LongPressDemo.ts",["./rollupPluginModLoBabelHelpers.js","cc"],(function(t){var n,o,s,e,r,i;return{setters:[function(t){n=t.inheritsLoose},function(t){o=t.cclegacy,s=t._decorator,e=t.Label,r=t.tween,i=t.v3}],execute:function(){var a;o._RF.push({},"08bdbQMsThDb4zbnXhIN7el","LongPressDemo",void 0);var c=s.ccclass;s.property,t("LongPressDemo",c("LongPressDemo")(a=function(t){function o(){for(var n,o=arguments.length,s=new Array(o),e=0;e<o;e++)s[e]=arguments[e];return(n=t.call.apply(t,[this].concat(s))||this).btnLongPress=null,n.data={count:0,totalCount:0},n}n(o,t);var s=o.prototype;return s.onEnterTransitionStart=function(t){var n=this;tnt.vm.observe(this),this.btnLongPress=this.find("btnLongPress");var o=this.findComponent("labelCount",e),s=this.findComponent("labelTotalCount",e);tnt.vm.label(this,o,"*.count"),tnt.vm.label(this,s,"*.totalCount"),tnt.touch.onLongPress(this.btnLongPress,(function(t){n.data.count=t,n.data.totalCount++,r(n.btnLongPress).to(.04,{scale:i(1.1,1.1,1.1)}).to(.04,{scale:i(1,1,1)}).start()}),this,.1)},s.onEnter=function(){},s.onExit=function(){},s.onExitTransitionStart=function(t){tnt.vm.violate(this),tnt.touch.offLongPress(this.btnLongPress)},o}(tnt.SceneBase))||a);o._RF.pop()}}}));

System.register("chunks:///_virtual/test-example",["./BaseDemo.ts","./LongPressDemo.ts"],(function(){return{setters:[null,null],execute:function(){}}}));

(function(r) {
  r('virtual:///prerequisite-imports/test-example', 'chunks:///_virtual/test-example'); 
})(function(mid, cid) {
    System.register(mid, [cid], function (_export, _context) {
    return {
        setters: [function(_m) {
            var _exportObj = {};

            for (var _key in _m) {
              if (_key !== "default" && _key !== "__esModule") _exportObj[_key] = _m[_key];
            }
      
            _export(_exportObj);
        }],
        execute: function () { }
    };
    });
});